package io.wren.value;

import io.wren.vm.WrenVM;

public class ObjClosure extends Obj {
	public ObjFn fn;
	public ObjUpvalue[] upvalues;
	
	public ObjClosure(WrenVM vm, ObjFn fn) {
		super(vm);
		this.fn = fn;
		this.upvalues = new ObjUpvalue[fn.numUpvalues];
	}
}
