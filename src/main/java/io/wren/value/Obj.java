package io.wren.value;

import io.wren.vm.WrenVM;

public class Obj {
	protected final WrenVM vm;
	public ObjClass classObj;

	public Obj(WrenVM vm) {
		this.vm = vm;
	}

	@Override
	public String toString() {
		if(classObj == null) return "Null ObjClass";
		return "instance of " + classObj.name;
	}
}
