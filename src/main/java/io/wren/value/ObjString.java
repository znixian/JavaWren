package io.wren.value;

import io.wren.vm.StringPool;
import io.wren.vm.WrenVM;

import java.util.ArrayList;
import java.util.List;

public class ObjString extends Obj {
	public String value;

	/**
	 * DO NOT USE - SEE {@link StringPool#get} INSTEAD!
	 *
	 * @param vm
	 * @param string
	 */
	public ObjString(WrenVM vm, String string) {
		super(vm);
		this.value = string;
		classObj = vm.stringClass;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ObjString)) return false;

		String s1 = value;
		String s2 = ((ObjString) obj).value;
		return s1.equals(s2);
	}

	@Override
	public String toString() {
		return value;
	}

	public int length() {
		return value.length();
	}
}
