package io.wren.value;

import io.wren.vm.WrenVM;

public class ObjRange extends Obj {

	public int from;
	public int to;
	public boolean isInclusive;

	public ObjRange(WrenVM vm, int from, int to, boolean isInclusive) {
		super(vm);
		this.from = from;
		this.to = to;
		this.isInclusive = isInclusive;

		classObj = vm.rangeClass;
	}

	@Override
	public boolean equals(Object obj) {
		throw new RuntimeException("TODO");
	}
}
