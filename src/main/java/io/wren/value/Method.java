package io.wren.value;

import io.wren.enums.MethodType;
import io.wren.vm.NativeMethod;

public class Method {

	public ObjFn getFn() {
		if (fn instanceof ObjClosure) {
			return ((ObjClosure) fn).fn;
		}
		return (ObjFn) fn;
	}

	public NativeMethod primitiveMethod;
	public ForeignMethod foreignMethod;
	public MethodType type;
	public Obj fn;

	/**
	 * When a foreign method is bound, this should be set to the module that
	 * method is in.
	 */
	public ObjModule module;
}
