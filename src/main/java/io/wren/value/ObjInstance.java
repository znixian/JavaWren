package io.wren.value;

import io.wren.vm.WrenVM;

public class ObjInstance extends Obj {
	public Value[] fields;
	
	public ObjInstance(WrenVM vm, ObjClass classObj) {
		super(vm);
		fields = new Value[classObj.numFields];
		for(int i = 0;i < classObj.numFields;i++) {
			fields[i] = Value.NULL;
		}
		
		this.classObj = classObj;
	}
}
