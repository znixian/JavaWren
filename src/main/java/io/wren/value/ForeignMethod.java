/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wren.value;

import io.wren.vm.*;

/**
 * Represents a foreign method - a method implemented in Java that can be called
 * from a Wren script.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public interface ForeignMethod {

	Value invokeForeign(MethodContext ctx, Value[] params);

	public static class MethodContext {

		private final WrenVM vm;
		private final Method method;
		private final ObjClass klass;
		private final Value instance;
		private final VMContext ctx;

		public MethodContext(WrenVM vm, Method method, ObjClass klass,
				Value instance, VMContext ctx) {
			this.vm = vm;
			this.method = method;
			this.klass = klass;
			this.instance = instance;
			this.ctx = ctx;
		}

		public WrenVM getVm() {
			return vm;
		}

		public Method getMethod() {
			return method;
		}

		public ObjClass getKlass() {
			return klass;
		}

		public Value getInstance() {
			return instance;
		}

		public VMContext getCtx() {
			return ctx;
		}

	}

}
