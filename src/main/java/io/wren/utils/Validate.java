package io.wren.utils;

import io.wren.value.Value;
import io.wren.vm.VMContext;

public class Validate {

	public static int Index(VMContext ctx, Value arg, int count, String argName) {
		if (!Num(ctx, arg, argName)) return Integer.MIN_VALUE;

		return IndexValue(ctx, count, arg.asInt(), argName);
	}

	public static boolean Num(VMContext ctx, Value arg, String argName) {
		if (arg.isNum()) return true;

		ctx.getCurrentFiber().error = new Value(ctx.vm, argName + " must be a number.");
		return false;
	}

	public static int IndexValue(VMContext ctx, int count, int value, String argName) {
		if (!IntValue(ctx, value, argName)) return Integer.MIN_VALUE;

		if (value < 0) value = count + value;

		if (value >= 0 && value < count) return value;

		ctx.getCurrentFiber().error = new Value(ctx.vm, argName + " out of bounds.");
		return Integer.MIN_VALUE;
	}

	public static boolean IntValue(VMContext ctx, double value, String argName) {
		if (value == (int) value) return true;

		ctx.getCurrentFiber().error = new Value(ctx.vm, argName + " must be an integer.");
		return false;
	}

	public static boolean Int(VMContext ctx, Value arg, String argName) {
		if (!Num(ctx, arg, argName)) return false;

		return IntValue(ctx, arg.asDouble(), argName);
	}

	public static boolean String(VMContext ctx, Value arg, String argName) {
		if (arg.isString()) return true;

		ctx.getCurrentFiber().error = new Value(ctx.vm, argName + " must be a string.");
		return false;
	}

	public static boolean Key(VMContext ctx, Value arg) {
		if (arg.isBool() || arg.isClass() || arg.isFiber() || arg.isNull() || arg.isNum() || arg.isRange() || arg.isString()) {
			return true;
		}

		ctx.getCurrentFiber().error = new Value(ctx.vm, "Key must be a value type or currentFiber.");
		return false;
	}

	public static boolean Fn(VMContext ctx, Value arg, String argName) {
		if (arg.isFn() || arg.isClosure()) return true;

		ctx.getCurrentFiber().error = new Value(ctx.vm, argName + " must be a function.");
		return false;
	}
}
