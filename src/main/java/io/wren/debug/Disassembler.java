/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wren.debug;

import io.wren.enums.Code;
import io.wren.utils.*;
import io.wren.value.*;
import io.wren.vm.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Disassembler {

	private final ByteBuffer bytecode;
	private final ObjModule module;
	private final List<Value> constants = new ArrayList<>();
	private final SymbolTable methodNames;
	private int ptr;

	public Disassembler(ByteBuffer bytecode, Buffer<Value> constants, ObjModule module) {
		this.bytecode = bytecode;
		this.module = module;

		methodNames = findVM(module).methodNames;

		constants.forEach(this.constants::add);

		while (ptr < bytecode.count()) {
			disassemble();
		}
	}

	public Disassembler(ObjFiber fiber) {
		ObjFn fn = fiber.frames.get(0).getFn();

		// TODO deduplicate
		this.bytecode = fn.bytecode;
		this.module = fn.module;
		methodNames = findVM(module).methodNames;
		fn.constants.forEach(constants::add);

		while (ptr < bytecode.count()) {
			disassemble();
		}
	}

	private WrenVM findVM(Obj module) {
		try {
			Field field = Obj.class.getDeclaredField("vm");
			field.setAccessible(true);
			WrenVM vm = (WrenVM) field.get(module);
			return vm;
		} catch (IllegalAccessException | NoSuchFieldException e) {
			throw new RuntimeException(e);
		}
	}

	private void disassemble() {
		// TODO replace SHORT() and BYTE() with VAR() and CONST() where applicable

		Code instruction = Code.fromByte(bytecode.get(ptr));
		System.out.print(String.format("%4X %1$4d: %s", ptr, instruction.name()));
		ptr++;
		switch (instruction) {
			case AND:
				SHORT();
				break;
			case CALL_0:
			case CALL_1:
			case CALL_2:
			case CALL_3:
			case CALL_4:
			case CALL_5:
			case CALL_6:
			case CALL_7:
			case CALL_8:
			case CALL_9:
			case CALL_10:
			case CALL_11:
			case CALL_12:
			case CALL_13:
			case CALL_14:
			case CALL_15:
			case CALL_16:
				METHOD();
				break;
			case SUPER_0:
			case SUPER_1:
			case SUPER_2:
			case SUPER_3:
			case SUPER_4:
			case SUPER_5:
			case SUPER_6:
			case SUPER_7:
			case SUPER_8:
			case SUPER_9:
			case SUPER_10:
			case SUPER_11:
			case SUPER_12:
			case SUPER_13:
			case SUPER_14:
			case SUPER_15:
			case SUPER_16:
				// TODO simplify
				METHOD();
				CONST();
				break;
			case CLASS:
				BYTE();
				break;
			case CLOSE_UPVALUE:
				break;
			case CLOSURE:
				throw new NotImplementedException();
			case CONSTANT:
				CONST();
				break;
			case CONSTRUCT:
				break;
			case DUP:
				break;
			case END:
				// throw new IllegalStateException("END opcode should be unreachable.");
				break;
			case FALSE:
				break;
			case FOREIGN_CLASS:
				break;
			case FOREIGN_CONSTRUCT:
				throw new NotImplementedException();
			case IMPORT_VARIABLE:
				CONST();
				CONST();
				break;
			case JUMP:
				SHORT();
				break;
			case JUMP_IF:
				SHORT();
				break;
			case LOAD_FIELD:
				SHORT();
				break;
			case LOAD_FIELD_THIS:
				BYTE();
				break;
			case LOAD_LOCAL:
				BYTE();
				break;
			case LOAD_LOCAL_0:
			case LOAD_LOCAL_1:
			case LOAD_LOCAL_2:
			case LOAD_LOCAL_3:
			case LOAD_LOCAL_4:
			case LOAD_LOCAL_5:
			case LOAD_LOCAL_6:
			case LOAD_LOCAL_7:
			case LOAD_LOCAL_8:
				break;
			case LOAD_MODULE:
				CONST();
				break;
			case LOAD_MODULE_VAR:
				VAR();
				break;
			case LOAD_UPVALUE:
				BYTE();
				break;
			case LOOP:
				SHORT();
				break;
			case METHOD_INSTANCE:
			case METHOD_STATIC:
				SHORT();
				break;
			case NULL:
				break;
			case OR:
				SHORT();
				break;
			case POP:
				break;
			case RETURN:
				break;
			case STORE_FIELD:
				BYTE();
				break;
			case STORE_FIELD_THIS:
				BYTE();
				break;
			case STORE_LOCAL:
				BYTE();
				break;
			case STORE_MODULE_VAR:
				VAR();
				break;
			case STORE_UPVALUE:
				BYTE();
				break;
			case TRUE:
				break;
			default:
				break;
		}

		System.out.println();
	}

	private void VAR() {
		System.out.print(" V/" + module.variableNames.get(READ_SHORT()));
	}

	private void METHOD() {
		int sym = READ_SHORT();
		System.out.format(" M/%4d/%s", sym, methodNames.get(sym));
	}

	private void CONST() {
		System.out.print(" C/" + constants.get(READ_SHORT()));
	}

	private void BYTE() {
		System.out.print(" B/" + READ_BYTE());
	}

	private void SHORT() {
		System.out.print(" S/" + READ_SHORT());
	}

	private short READ_BYTE() {
		short x = bytecode.get(ptr++);
		if (x < 0) {
			x += 256; // make the value unsigned
		}
		return x;
	}

	private int READ_SHORT() {
		return (READ_BYTE() << 8 | READ_BYTE());
	}

}
