package io.wren.libs;

import io.wren.utils.Wren;
import io.wren.value.Value;
import io.wren.vm.VMContext;

public class WrenNull {
	public static boolean not(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(stack, stackStart, true);
	}
	
	public static boolean to_string(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(ctx, stack, stackStart, "null");
	}
}
