package io.wren.libs;

import io.wren.utils.Validate;
import io.wren.utils.Wren;
import io.wren.value.ObjFn;
import io.wren.value.Value;
import io.wren.vm.VMContext;

public class WrenFn {
	public static boolean arity(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFn fn = stack[stackStart].asFn();
		return Wren.RETURN(stack, stackStart, fn.arity);
	}

	public static boolean to_string(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(ctx, stack, stackStart, "<fn>");
	}

	// METACLASS

	public static boolean new_fn(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value arg = stack[stackStart + 1];

		if (!Validate.Fn(ctx, arg, "Argument")) return false;

		return Wren.RETURN(stack, stackStart, arg);
	}
}
