package io.wren.libs;

import io.wren.utils.Validate;
import io.wren.utils.Wren;
import io.wren.value.ObjRange;
import io.wren.value.ObjString;
import io.wren.value.Value;
import io.wren.vm.VMContext;

public class WrenNum {
	public static boolean sub(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		double x = left.asDouble() - right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean add(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		double x = left.asDouble() + right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean mul(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		double x = left.asDouble() * right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean div(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		double x = left.asDouble() / right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean lt(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		boolean x = left.asDouble() < right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean gt(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		boolean x = left.asDouble() > right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean le(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		boolean x = left.asDouble() <= right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean ge(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		boolean x = left.asDouble() >= right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean band(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		int x = left.asInt() & right.asInt();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean bor(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		int x = left.asInt() | right.asInt();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean xor(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		int x = left.asInt() ^ right.asInt();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean lshift(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		int x = left.asInt() << right.asInt();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean rshift(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		int x = left.asInt() >> right.asInt();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean abs(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.abs(num.asDouble()));
	}

	public static boolean acos(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.acos(num.asDouble()));
	}

	public static boolean asin(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.asin(num.asDouble()));
	}

	public static boolean atan(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.atan(num.asDouble()));
	}

	public static boolean ceil(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.ceil(num.asDouble()));
	}

	public static boolean cos(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.cos(num.asDouble()));
	}

	public static boolean floor(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.floor(num.asDouble()));
	}

	public static boolean minus(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, -(num.asDouble()));
	}

	public static boolean sin(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.sin(num.asDouble()));
	}

	public static boolean sqrt(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.sqrt(num.asDouble()));
	}

	public static boolean tan(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, Math.tan(num.asDouble()));
	}

	public static boolean mod(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right operand")) return false;
		double x = left.asDouble() % right.asDouble();
		return Wren.RETURN(stack, stackStart, x);
	}

	public static boolean tilde(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, ~(num.asInt()));
	}

	public static boolean dotdot(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right hand side of range")) return false;

		int l = left.asInt();
		int r = right.asInt();
		ObjRange range = new ObjRange(ctx.vm, l, r, true);
		return Wren.RETURN(stack, stackStart, range);
	}

	public static boolean dotdotdot(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!Validate.Num(ctx, right, "Right hand side of range")) return false;

		int l = left.asInt();
		int r = right.asInt();
		ObjRange range = new ObjRange(ctx.vm, l, r, false);
		return Wren.RETURN(stack, stackStart, range);
	}

	public static boolean atan2(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		return Wren.RETURN(stack, stackStart, (Math.atan2(left.asDouble(), right.asDouble())));
	}

	public static boolean fraction(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		double d = num.asDouble();
		double n = d - (int) d;
		return Wren.RETURN(stack, stackStart, n);
	}

	public static boolean is_infinity(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		double d = num.asDouble();
		return Wren.RETURN(stack, stackStart, Double.isInfinite(d));
	}

	public static boolean is_integer(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		double d = num.asDouble();
		if (!Double.isFinite(d)) return Wren.RETURN(stack, stackStart, false);

		return Wren.RETURN(stack, stackStart, ((int) d == d));
	}

	public static boolean is_nan(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		double d = num.asDouble();
		return Wren.RETURN(stack, stackStart, Double.isNaN(d));
	}

	public static boolean sign(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		double n = num.asDouble();
		double ret = 0;
		if (n < 0)
			ret = -1;
		else if (n > 0) ret = 1;
		return Wren.RETURN(stack, stackStart, ret);
	}

	public static boolean to_string(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(ctx, stack, stackStart, "" + num.asDouble());
	}

	public static boolean truncate(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value num = stack[stackStart];
		return Wren.RETURN(stack, stackStart, num.asInt());
	}

	public static boolean eqeq(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!right.isNum()) {
			return Wren.RETURN(stack, stackStart, false);
		}
		return Wren.RETURN(stack, stackStart, left.asDouble() == right.asDouble());
	}

	public static boolean bangeq(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		if (!right.isNum()) {
			return Wren.RETURN(stack, stackStart, false);
		}
		return Wren.RETURN(stack, stackStart, left.asDouble() != right.asDouble());
	}

	// METACLASS

	public static boolean pi(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(stack, stackStart, Math.PI);
	}

	public static boolean fromString(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value arg1 = stack[stackStart + 1];

		if (!Validate.String(ctx, arg1, "Argument")) return false;

		ObjString str = arg1.asString();

		try {
			double n = Double.parseDouble(str.value);
			return Wren.RETURN(stack, stackStart, n);
		} catch (NumberFormatException e) {
			ctx.getCurrentFiber().error = new Value(ctx.vm, "Number literal is too large.");
			return false;
		}
	}
}
