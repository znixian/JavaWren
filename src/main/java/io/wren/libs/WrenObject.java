package io.wren.libs;

import io.wren.utils.Wren;
import io.wren.value.*;
import io.wren.vm.VMContext;

public class WrenObject {
	public static boolean not(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(stack, stackStart, false);
	}

	public static boolean eqeq(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		return Wren.RETURN(stack, stackStart, left.equals(right));
	}

	public static boolean bangeq(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		return Wren.RETURN(stack, stackStart, !left.equals(right));
	}

	public static boolean is(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];

		if (!right.isClass()) {
			return Wren.RETURN_ERROR(ctx, "Right operand must be a class.");
		}

		ObjClass classObj = ctx.vm.getInlineClass(left);
		ObjClass baseClassObj = right.asClass();

		do {
			if (baseClassObj == classObj)
				return Wren.RETURN(stack, stackStart, true);

			classObj = classObj.superclass;
		} while (classObj != null);

		return Wren.RETURN(stack, stackStart, false);
	}

	public static boolean to_string(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Obj obj = stack[stackStart].asObj();
		String string = obj.classObj.toString();
		return Wren.RETURN(ctx, stack, stackStart, string);
	}

	public static boolean type(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(stack, stackStart, ctx.vm.getInlineClass(stack[stackStart]));
	}

	public static boolean same(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value left = stack[stackStart];
		Value right = stack[stackStart + 1];
		return Wren.RETURN(stack, stackStart, left.equals(right));
	}
}
