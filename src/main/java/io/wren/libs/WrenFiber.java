package io.wren.libs;

import io.wren.utils.Validate;
import io.wren.utils.Wren;
import io.wren.value.ObjFiber;
import io.wren.value.Value;
import io.wren.vm.VMContext;

public class WrenFiber {
	private static boolean runFiber(VMContext ctx, ObjFiber fiber, Value[] stack, int stackStart, boolean isCall, boolean hasValue) {
		if (isCall) {
			if (fiber.caller != null) Wren.RETURN_ERROR(ctx, "Fiber has already been called.");

			fiber.caller = ctx.getCurrentFiber();
		}

		if (fiber.frames.isEmpty()) {
			ctx.getCurrentFiber().error = new Value(ctx.vm, "Cannot " + (isCall ? "call" : "transfer to") + " a finished currentFiber.");
			return false;
		}

		if (fiber.error != null) {
			ctx.getCurrentFiber().error = new Value(ctx.vm, "Cannot " + (isCall ? "call" : "transfer to") + " an aborted currentFiber.");
			return false;
		}

		if (hasValue) ctx.getCurrentFiber().stackTop--;

		if (fiber.stackTop > 0) {
			fiber.stack[fiber.stackTop - 1] = hasValue ? stack[stackStart + 1] : Value.NULL;
		}

		ctx.setCurrentFiber(fiber);

		return false;
	}

	public static boolean call(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber fiber = stack[stackStart].asFiber();
		return runFiber(ctx, fiber, stack, stackStart, true, false);
	}

	public static boolean call1(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber fiber = stack[stackStart].asFiber();
		return runFiber(ctx, fiber, stack, stackStart, true, true);
	}

	public static boolean error(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber fiber = stack[stackStart].asFiber();
		return Wren.RETURN(stack, stackStart, fiber.error);
	}

	public static boolean is_done(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber runFiber = stack[stackStart].asFiber();
		return Wren.RETURN(stack, stackStart, runFiber.frames.isEmpty() || runFiber.error != null);
	}

	public static boolean transfer(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber fiber = stack[stackStart].asFiber();
		return runFiber(ctx, fiber, stack, stackStart, false, false);
	}

	public static boolean transfer1(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber fiber = stack[stackStart].asFiber();
		return runFiber(ctx, fiber, stack, stackStart, false, true);
	}

	public static boolean transfer_error(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber fiber = stack[stackStart].asFiber();
		runFiber(ctx, fiber, stack, stackStart, false, true);
		ctx.getCurrentFiber().error = stack[stackStart + 1];
		return false;
	}

	public static boolean try_call(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber current = ctx.getCurrentFiber();
		ObjFiber tried = stack[stackStart].asFiber();

		if (tried.frames.isEmpty()) Wren.RETURN_ERROR(ctx, "Cannot try a finished currentFiber.");
		if (tried.caller != null) Wren.RETURN_ERROR(ctx, "Fiber has already been called.");

		ctx.setCurrentFiber(tried);

		tried.caller = current;
		tried.callerIsTrying = true;

		if (tried.stackTop > 0) {
			tried.stack[tried.stackTop - 1] = Value.NULL;
		}

		return false;
	}

	// METACLASS

	public static boolean new_fiber(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value arg = stack[stackStart + 1];

		if (!Validate.Fn(ctx, arg, "Argument")) return false;

		ObjFiber newFiber = new ObjFiber(ctx.vm, arg.asObj());

		newFiber.stack[0] = Value.NULL;
		newFiber.stackTop++;

		return Wren.RETURN(stack, stackStart, newFiber);
	}

	public static boolean abort(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		Value arg = stack[stackStart + 1];
		ctx.getCurrentFiber().error = arg;

		return arg.isNull();
	}

	public static boolean current(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		return Wren.RETURN(stack, stackStart, ctx.getCurrentFiber());
	}

	public static boolean suspend(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ctx.setCurrentFiber(null);
		return false;
	}

	public static boolean yield(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber current = ctx.getCurrentFiber();
		ctx.setCurrentFiber(current.caller);

		current.caller = null;
		current.callerIsTrying = false;

		if (ctx.getCurrentFiber() != null) {
			ctx.getCurrentFiber().stack[ctx.getCurrentFiber().stackTop - 1] = Value.NULL;
		}

		return false;
	}

	public static boolean yield1(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjFiber current = ctx.getCurrentFiber();
		ctx.setCurrentFiber(current.caller);

		current.caller = null;
		current.callerIsTrying = false;

		if (ctx.getCurrentFiber() != null) {
			ctx.getCurrentFiber().stack[ctx.getCurrentFiber().stackTop - 1] = stack[stackStart + 1];
			current.stackTop--;
		}

		return false;
	}
}
