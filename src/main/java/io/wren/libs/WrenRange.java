/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wren.libs;

import io.wren.utils.Validate;
import io.wren.utils.Wren;
import io.wren.value.ObjRange;
import io.wren.value.Value;
import io.wren.vm.VMContext;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class WrenRange {

	public static boolean from(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange list = stack[stackStart].asRange();
		return Wren.RETURN(stack, stackStart, list.from);
	}

	public static boolean to(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange list = stack[stackStart].asRange();
		return Wren.RETURN(stack, stackStart, list.to);
	}

	public static boolean min(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange list = stack[stackStart].asRange();
		return Wren.RETURN(stack, stackStart, Math.min(list.from, list.to));
	}

	public static boolean max(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange list = stack[stackStart].asRange();
		return Wren.RETURN(stack, stackStart, Math.max(list.from, list.to));
	}

	public static boolean isInclusive(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange list = stack[stackStart].asRange();
		return Wren.RETURN(stack, stackStart, list.isInclusive);
	}

	public static boolean iterate(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange range = stack[stackStart].asRange();
		Value iter = stack[stackStart + 1];
		int index;

		if (iter.isNull()) {
			index = 0;
		} else if (!Validate.Int(ctx, iter, "Iterator")) {
			return false;
		} else {
			index = iter.asInt() + 1;
		}

		int length = Math.abs(range.from - range.to) + (range.isInclusive ? 1 : 0);
		if (index < 0 || index >= length) {
			return Wren.RETURN(stack, stackStart, false);
		}

		return Wren.RETURN(stack, stackStart, index);
	}

	public static boolean iteratorValue(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange range = stack[stackStart].asRange();
		int length = Math.abs(range.from - range.to) + (range.isInclusive ? 1 : 0);
		Value arg1 = stack[stackStart + 1];

		int index = Validate.Index(ctx, arg1, length, "Iterator");
		if (index == Integer.MIN_VALUE) {
			return false;
		}

		if (length == 0) {
			return Wren.RETURN(stack, stackStart, range.from);
		}

		int dir = (range.to > range.from) ? 1 : -1;
		int result = range.from + dir * index;
		return Wren.RETURN(stack, stackStart, result);
	}

	public static boolean toString(VMContext ctx, Value[] stack, int stackStart, int numArgs) {
		ObjRange list = stack[stackStart].asRange();
		return Wren.RETURN(ctx, stack, stackStart, String.format(
			"Range(%d-%d, %b)", list.from, list.to, list.isInclusive));
	}

}
