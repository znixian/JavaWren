package io.wren.vm;

import io.wren.enums.*;
import io.wren.libs.*;
import io.wren.utils.*;
import io.wren.value.*;

import java.util.function.Consumer;

public class WrenVM {

	private final StringPool strings;
	private final ObjMap modules;
	private final FFI ffi;

	public SymbolTable methodNames;

	ObjClass objectClass;
	ObjClass classClass;
	ObjClass boolClass;
	ObjClass nullClass;
	ObjClass numClass;

	public ObjClass listClass;
	public ObjClass mapClass;
	public ObjClass stringClass;
	public ObjClass fiberClass;
	public ObjClass rangeClass;
	public ObjClass fnClass;

	public WrenVM() {
		modules = new ObjMap(this);
		methodNames = new SymbolTable();
		strings = new StringPool(this);
		ffi = new FFI();

		ObjModule coreModule = new ObjModule(this, "core");
		modules.set(Value.NULL, new Value(coreModule));

		initializeCore();
	}

	ObjModule getOrLoadModule(Value name) {
		ObjModule module = getModule(name);
		if (module != null) {
			return module;
		}

		return ffi.loadModule(this, name.asString().value);
	}

	ObjModule getModule(Value name) {
		Value moduleValue = modules.get(name);
		if (moduleValue.isUndefined()) {
			return null;
		}
		return moduleValue.asModule();
	}

	/**
	 * Get the core module, where stuff can be put to be available to all
	 * scripts.
	 *
	 * TODO make this private once we have the proper FFI working.
	 *
	 * @return The core module.
	 */
	public ObjModule getCoreModule() {
		ObjModule module = getModule(Value.NULL);
		if (module == null) {
			throw new RuntimeException("NO CORE MODULE");
		}
		return module;
	}

	public FFI getFFI() {
		return ffi;
	}

	private Value importModule(String name) {
		throw new NotImplementedException();
	}

	private Value findVariable(ObjModule module, String name) {
		int symbol = module.variableNames.find(name);
		return module.variables.get(symbol);
	}

	private ObjFn compile(ObjModule module, String sourcePath, String source, Consumer<String> errorOutput) {
		Parser parser = new Parser();
		parser.vm = this;
		parser.module = module;
		parser.sourcePath = sourcePath;
		parser.source = source;
		parser.value = Value.UNDEFINED;

		parser.tokenStart = 0;
		parser.currentChar = 0;
		parser.currentLine = 1;

		parser.current = new Token();

		parser.skipNewlines = true;
		parser.errorOutput = errorOutput;
		parser.hasError = false;

		parser.nextToken();

		Compiler compiler = new Compiler(this, parser, null, true);
		compiler.ignoreNewlines();

		while (!compiler.match(TokenType.EOF)) {
			compiler.definition();

			if (!compiler.matchLine()) {
				compiler.consume(TokenType.EOF, "Expect end of file.");
				break;
			}
		}

		compiler.emit(Code.NULL);
		compiler.emit(Code.RETURN);

		Buffer<Value> variables = parser.module.variables;
		SymbolTable variableNames = parser.module.variableNames;
		for (int i = 0; i < variables.count(); i++) {
			if (variables.get(i).isUndefined()) {
				compiler.error(String.format("Variable '%s' is used but not defined.", variableNames.get(i)));
			}
		}

		return compiler.endCompiler("(script)");
	}

	public ObjString str(String str) {
		return strings.get(str);
	}

	public WrenInterpretResult interpret(String moduleName, String source) {
		ObjFiber fiber = loadModule(moduleName, source);
		if (fiber == null) {
			return WrenInterpretResult.RESULT_COMPILE_ERROR;
		}

		return runInterpreter(fiber);
	}

	public ObjModule interpretModule(String name, String source) {
		WrenInterpretResult res = interpret(name, source);
		if (res == WrenInterpretResult.RESULT_COMPILE_ERROR) {
			throw new IllegalStateException("Module did not compile!");
		} else if (res == WrenInterpretResult.RESULT_RUNTIME_ERROR) {
			throw new IllegalStateException("Module ran with error!");
		}

		// TODO potentially more robust module loading
		ObjModule mod = getModule(new Value(this, name));
		if (mod == null) {
			throw new IllegalStateException("Module " + name + " resolves to null");
		}
		return mod;
	}

	public WrenInterpretResult runInterpreter(ObjFiber fiber) {
		VMContext context = new VMContext(this, fiber);

		WrenInterpretResult result;
		do {
			result = context.runStep();
		} while (result == null);

		return result;
	}

	Value validateSuperclass(Value name, ObjClass superclass, int numFields) {
		if (superclass == classClass || superclass == fiberClass || superclass == fnClass || superclass == listClass || superclass == mapClass || superclass == rangeClass || superclass == stringClass) {
			return new Value(this, "Class '" + name + "' cannot inherit from built-in class '" + superclass.name + "'.");
		}

		if (superclass.numFields == -1) {
			return new Value(this, "Class '" + name + "' cannot inherit from foreign class '" + superclass.name + "'.");
		}

		if (superclass.numFields + numFields > ClassCompiler.MAX_FIELDS) {
			return new Value(this, "Class '" + name + "' may not have more than 255 fields, including inherited ones.");
		}

		return null;
	}

	private WrenInterpretResult loadIntoCore(String source) {
		ObjModule coreModule = getCoreModule();

		ObjFn func = compile(coreModule, "", source, System.err::println);
		if (func == null) {
			return WrenInterpretResult.RESULT_COMPILE_ERROR;
		}

		ObjFiber fiber = new ObjFiber(this, func);
		return runInterpreter(fiber);
	}

	public ObjFiber loadModule(String name, String source) {
		ObjModule module = getModule(new Value(this, name));

		if (module == null) {
			module = new ObjModule(this, name);

			modules.set(new Value(this, name), new Value(module));

			ObjModule coreModule = getCoreModule();
			for (int i = 0; i < coreModule.variables.count(); i++) {
				module.defineVariable(coreModule.variableNames.get(i), coreModule.variables.get(i));
			}
		}

		ObjFn func = compile(module, name, source, System.err::println);
		if (func == null) {
			return null;
		}

		return new ObjFiber(this, func);
	}

	private void initializeCore() {
		ObjModule coreModule = getCoreModule();

		// object
		objectClass = defineClass(coreModule, "Object");
		objectClass.bindMethod(methodNames.ensure("!"), WrenObject::not);
		objectClass.bindMethod(methodNames.ensure("==(_)"), WrenObject::eqeq);
		objectClass.bindMethod(methodNames.ensure("!=(_)"), WrenObject::bangeq);
		objectClass.bindMethod(methodNames.ensure("is(_)"), WrenObject::is);
		objectClass.bindMethod(methodNames.ensure("toString"), WrenObject::to_string);
		objectClass.bindMethod(methodNames.ensure("type"), WrenObject::type);

		// class
		classClass = defineClass(coreModule, "Class");
		classClass.bindMethod(methodNames.ensure("name"), WrenClass::name);
		classClass.bindMethod(methodNames.ensure("supertype"), WrenClass::supertype);
		classClass.bindMethod(methodNames.ensure("toString"), WrenClass::to_string);
		classClass.bindSuperclass(objectClass);

		ObjClass objectMetaclass = defineClass(coreModule, "Object metaclass");

		objectClass.classObj = objectMetaclass;
		objectMetaclass.classObj = classClass;
		classClass.classObj = classClass;

		objectMetaclass.bindSuperclass(classClass);
		objectMetaclass.bindStaticMethod(methodNames.ensure("same(_,_)"), WrenObject::same);

		WrenInterpretResult ret = loadIntoCore(Core.source);
		if (ret != WrenInterpretResult.RESULT_SUCCESS) {
			System.out.println("error compiling core.wren");
			throw new RuntimeException("error compiling core.wren");
		}

		// bool
		boolClass = findVariable(coreModule, "Bool").asClass();
		boolClass.bindMethod(methodNames.ensure("toString"), WrenBool::to_string);
		boolClass.bindMethod(methodNames.ensure("!"), WrenBool::not);

		// fiber
		fiberClass = findVariable(coreModule, "Fiber").asClass();
		fiberClass.classObj.bindStaticMethod(methodNames.ensure("new(_)"), WrenFiber::new_fiber);
		fiberClass.classObj.bindStaticMethod(methodNames.ensure("abort(_)"), WrenFiber::abort);
		fiberClass.classObj.bindStaticMethod(methodNames.ensure("current"), WrenFiber::current);
		fiberClass.classObj.bindStaticMethod(methodNames.ensure("suspend()"), WrenFiber::suspend);
		fiberClass.classObj.bindStaticMethod(methodNames.ensure("yield()"), WrenFiber::yield);
		fiberClass.classObj.bindStaticMethod(methodNames.ensure("yield(_)"), WrenFiber::yield1);
		fiberClass.bindMethod(methodNames.ensure("call()"), WrenFiber::call);
		fiberClass.bindMethod(methodNames.ensure("call(_)"), WrenFiber::call1);
		fiberClass.bindMethod(methodNames.ensure("error"), WrenFiber::error);
		fiberClass.bindMethod(methodNames.ensure("isDone"), WrenFiber::is_done);
		fiberClass.bindMethod(methodNames.ensure("transfer()"), WrenFiber::transfer);
		fiberClass.bindMethod(methodNames.ensure("transfer(_)"), WrenFiber::transfer1);
		fiberClass.bindMethod(methodNames.ensure("transferError(_)"), WrenFiber::transfer_error);
		fiberClass.bindMethod(methodNames.ensure("try()"), WrenFiber::try_call);

		// fn
		fnClass = findVariable(coreModule, "Fn").asClass();
		fnClass.classObj.bindStaticMethod(methodNames.ensure("new(_)"), WrenFn::new_fn);
		fnClass.bindMethod(methodNames.ensure("arity"), WrenFn::arity);
		fnClass.bindMethod(methodNames.ensure("call()"));
		fnClass.bindMethod(methodNames.ensure("call(_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("call(_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_)"));
		fnClass.bindMethod(methodNames.ensure("toString"), WrenFn::to_string);

		// null
		nullClass = findVariable(coreModule, "Null").asClass();
		nullClass.bindMethod(methodNames.ensure("!"), WrenNull::not);
		nullClass.bindMethod(methodNames.ensure("toString"), WrenNull::to_string);

		// number
		numClass = findVariable(coreModule, "Num").asClass();
		numClass.classObj.bindStaticMethod(methodNames.ensure("fromString(_)"), WrenNum::fromString);
		numClass.classObj.bindStaticMethod(methodNames.ensure("pi"), WrenNum::pi);
		numClass.bindMethod(methodNames.ensure("-(_)"), WrenNum::sub);
		numClass.bindMethod(methodNames.ensure("+(_)"), WrenNum::add);
		numClass.bindMethod(methodNames.ensure("*(_)"), WrenNum::mul);
		numClass.bindMethod(methodNames.ensure("/(_)"), WrenNum::div);
		numClass.bindMethod(methodNames.ensure("<(_)"), WrenNum::lt);
		numClass.bindMethod(methodNames.ensure(">(_)"), WrenNum::gt);
		numClass.bindMethod(methodNames.ensure("<=(_)"), WrenNum::le);
		numClass.bindMethod(methodNames.ensure(">=(_)"), WrenNum::ge);
		numClass.bindMethod(methodNames.ensure("&(_)"), WrenNum::band);
		numClass.bindMethod(methodNames.ensure("|(_)"), WrenNum::bor);
		numClass.bindMethod(methodNames.ensure("^(_)"), WrenNum::xor);
		numClass.bindMethod(methodNames.ensure("<<(_)"), WrenNum::lshift);
		numClass.bindMethod(methodNames.ensure(">>(_)"), WrenNum::rshift);
		numClass.bindMethod(methodNames.ensure("abs"), WrenNum::abs);
		numClass.bindMethod(methodNames.ensure("acos"), WrenNum::acos);
		numClass.bindMethod(methodNames.ensure("asin"), WrenNum::asin);
		numClass.bindMethod(methodNames.ensure("atan"), WrenNum::atan);
		numClass.bindMethod(methodNames.ensure("ceil"), WrenNum::ceil);
		numClass.bindMethod(methodNames.ensure("cos"), WrenNum::cos);
		numClass.bindMethod(methodNames.ensure("floor"), WrenNum::floor);
		numClass.bindMethod(methodNames.ensure("-"), WrenNum::minus);
		numClass.bindMethod(methodNames.ensure("sin"), WrenNum::sin);
		numClass.bindMethod(methodNames.ensure("sqrt"), WrenNum::sqrt);
		numClass.bindMethod(methodNames.ensure("tan"), WrenNum::tan);
		numClass.bindMethod(methodNames.ensure("%(_)"), WrenNum::mod);
		numClass.bindMethod(methodNames.ensure("~"), WrenNum::tilde);
		numClass.bindMethod(methodNames.ensure("..(_)"), WrenNum::dotdot);
		numClass.bindMethod(methodNames.ensure("...(_)"), WrenNum::dotdotdot);
		numClass.bindMethod(methodNames.ensure("atan(_)"), WrenNum::atan);
		numClass.bindMethod(methodNames.ensure("fraction"), WrenNum::fraction);
		numClass.bindMethod(methodNames.ensure("isInfinity"), WrenNum::is_infinity);
		numClass.bindMethod(methodNames.ensure("isInteger"), WrenNum::is_integer);
		numClass.bindMethod(methodNames.ensure("isNan"), WrenNum::is_nan);
		numClass.bindMethod(methodNames.ensure("sign"), WrenNum::sign);
		numClass.bindMethod(methodNames.ensure("toString"), WrenNum::to_string);
		numClass.bindMethod(methodNames.ensure("truncate"), WrenNum::truncate);

		numClass.bindMethod(methodNames.ensure("==(_)"), WrenNum::eqeq);
		numClass.bindMethod(methodNames.ensure("!=(_)"), WrenNum::bangeq);

		// string
		stringClass = findVariable(coreModule, "String").asClass();
		stringClass.classObj.bindStaticMethod(methodNames.ensure("fromCodePoint(_)"), WrenString::from_code_point);
		stringClass.bindMethod(methodNames.ensure("+(_)"), WrenString::plus);
		stringClass.bindMethod(methodNames.ensure("[_]"), WrenString::subscript);
		stringClass.bindMethod(methodNames.ensure("byteAt_(_)"), WrenString::byte_at);
		stringClass.bindMethod(methodNames.ensure("byteCount_"), WrenString::byte_count);
		stringClass.bindMethod(methodNames.ensure("codePointAt_(_)"), WrenString::code_point_at);
		stringClass.bindMethod(methodNames.ensure("contains(_)"), WrenString::contains);
		stringClass.bindMethod(methodNames.ensure("endsWith(_)"), WrenString::ends_with);
		stringClass.bindMethod(methodNames.ensure("indexOf(_)"), WrenString::index_of);
		stringClass.bindMethod(methodNames.ensure("iterate(_)"), WrenString::iterate);
		stringClass.bindMethod(methodNames.ensure("iterateByte_(_)"), WrenString::iterate_byte);
		stringClass.bindMethod(methodNames.ensure("iteratorValue(_)"), WrenString::iterator_value);
		stringClass.bindMethod(methodNames.ensure("startsWith(_)"), WrenString::starts_with);
		stringClass.bindMethod(methodNames.ensure("toString"), WrenString::to_string);

		// list
		listClass = findVariable(coreModule, "List").asClass();
		listClass.classObj.bindStaticMethod(methodNames.ensure("new()"), WrenList::new_list);
		listClass.bindMethod(methodNames.ensure("[_]"), WrenList::subscript);
		listClass.bindMethod(methodNames.ensure("[_]=(_)"), WrenList::subscript_setter);
		listClass.bindMethod(methodNames.ensure("add(_)"), WrenList::add);
		listClass.bindMethod(methodNames.ensure("clear()"), WrenList::clear);
		listClass.bindMethod(methodNames.ensure("count"), WrenList::count);
		listClass.bindMethod(methodNames.ensure("insert(_,_)"), WrenList::insert);
		listClass.bindMethod(methodNames.ensure("iterate(_)"), WrenList::iterate);
		listClass.bindMethod(methodNames.ensure("iteratorValue(_)"), WrenList::iterator_value);
		listClass.bindMethod(methodNames.ensure("removeAt(_)"), WrenList::remove_at);

		// map
		mapClass = findVariable(coreModule, "Map").asClass();
		mapClass.classObj.bindStaticMethod(methodNames.ensure("new()"), WrenMap::new_map);
		mapClass.bindMethod(methodNames.ensure("[_]"), WrenMap::subscript);
		mapClass.bindMethod(methodNames.ensure("[_]=(_)"), WrenMap::subscript_setter);
		mapClass.bindMethod(methodNames.ensure("clear()"), WrenMap::clear);
		mapClass.bindMethod(methodNames.ensure("containsKey(_)"), WrenMap::contains_key);
		mapClass.bindMethod(methodNames.ensure("count"), WrenMap::count);
		mapClass.bindMethod(methodNames.ensure("remove(_)"), WrenMap::remove);
		mapClass.bindMethod(methodNames.ensure("iterate_(_)"), WrenMap::iterate);
		mapClass.bindMethod(methodNames.ensure("keyIteratorValue_(_)"), WrenMap::key_iterator_value);
		mapClass.bindMethod(methodNames.ensure("valueIteratorValue_(_)"), WrenMap::value_iterator_value);

		// range
		rangeClass = findVariable(coreModule, "Range").asClass();
		rangeClass.bindMethod(methodNames.ensure("from"), WrenRange::from);
		rangeClass.bindMethod(methodNames.ensure("to"), WrenRange::to);
		rangeClass.bindMethod(methodNames.ensure("min"), WrenRange::min);
		rangeClass.bindMethod(methodNames.ensure("max"), WrenRange::max);
		rangeClass.bindMethod(methodNames.ensure("isInclusive"), WrenRange::isInclusive);
		rangeClass.bindMethod(methodNames.ensure("iterate(_)"), WrenRange::iterate);
		rangeClass.bindMethod(methodNames.ensure("iteratorValue(_)"), WrenRange::iteratorValue);
		rangeClass.bindMethod(methodNames.ensure("toString"), WrenRange::toString);

		// system
		ObjClass systemClass = findVariable(coreModule, "System").asClass();
		systemClass.classObj.bindStaticMethod(methodNames.ensure("clock"), WrenSystem::clock);
		systemClass.classObj.bindStaticMethod(methodNames.ensure("writeString_(_)"), WrenSystem::write_string);

		// assign string class to created objects.
		strings.updateClasses(stringClass);

		// Initialize Meta
		loadIntoCore(Meta.source);
		ObjClass metaClass = findVariable(coreModule, "Meta").asClass();
		// TODO change to primitive method
		// metaClass.classObj.bindStaticMethod(methodNames.ensure("eval(_)"), WrenMeta::eval);
	}

	/**
	 * Create a class, and put it into the given module.
	 *
	 * @param module The module the class should belong to.
	 * @param name The name of the class that should be defined.
	 * @return The class that has been created.
	 */
	// TODO: move to seperate API class
	public ObjClass apiDefineClass(ObjModule module, String name) {
		ObjClass classObj = newClass(objectClass, 0, name);
		module.defineVariable(name, new Value(classObj));
		return classObj;
	}

	private ObjClass defineClass(ObjModule module, String name) {
		ObjClass classObj = newSingleClass(0, name);
		module.defineVariable(name, new Value(classObj));
		return classObj;
	}

	private ObjClass newSingleClass(int numFields, String name) {
		ObjClass classObj = new ObjClass(this, name);
		classObj.numFields = numFields;
		return classObj;
	}

	ObjClass newClass(ObjClass superclass, int numFields, String name) {
		ObjClass metaclass = newSingleClass(0, name + " metaclass");
		metaclass.classObj = classClass;

		metaclass.bindSuperclass(classClass);

		ObjClass classObj = newSingleClass(numFields, name);

		classObj.classObj = metaclass;
		classObj.bindSuperclass(superclass);

		return classObj;
	}

	public ObjClass getInlineClass(Value value) {
		switch (value.getType()) {
			case FALSE:
			case TRUE:
				return boolClass;
			case NULL:
				return nullClass;
			case NUM:
				return numClass;
			case OBJ:
				return value.asObj().classObj;
			case UNDEFINED:
			default:
				throw new UnreachableCodeException();
		}
	}
}
