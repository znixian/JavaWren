package io.wren.vm;

import io.wren.utils.Wren;
import io.wren.value.Value;

public interface NativeMethod {

	/**
	 * Invoke this primitive method.
	 *
	 * There are a few easy things you can get out of the stack easily:
	 * <ul>
	 * <li>{@code this} is available at {@code stack[stackStart]}</li>
	 * <li>Argument {@code n} is available at {@code stack[stackStart]}</li>
	 * <li>To return a value, put it at {@code stack[stackStart]}</li>
	 * </ul>
	 *
	 * @see Wren#RETURN(io.wren.value.Value[], int, io.wren.value.Value)
	 * @param ctx The context to run in
	 * @param stack The stack of the context. Provided for convenience, can be
	 * found (?) from the context/
	 * @param stackStart The position this method's stuff starts at on the
	 * stack. {@code stack[stackStart]} is {
	 * @this}, and {@code stack[stackStart+n]} is the {@code n}th argument.
	 * @param numArgs The number of arguments supplied to this method.
	 * @return {@code true} if the calling function should handle the stack,
	 * removing the arguments afterwards. {@code false} otherwise. Note that if
	 * you're not dealing with changing fibers or anything similar, you should
	 * return {@code true}.
	 */
	boolean invokePrimitive(VMContext ctx, Value[] stack, int stackStart, int numArgs);

}
