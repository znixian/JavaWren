package io.wren.vm;

import io.wren.enums.Code;
import io.wren.enums.MethodType;
import io.wren.enums.WrenInterpretResult;
import io.wren.utils.ByteBuffer;
import io.wren.utils.UnreachableCodeException;
import io.wren.value.CallFrame;
import io.wren.value.ForeignMethod;
import io.wren.value.Method;
import io.wren.value.Obj;
import io.wren.value.ObjClass;
import io.wren.value.ObjClosure;
import io.wren.value.ObjFiber;
import io.wren.value.ObjFn;
import io.wren.value.ObjInstance;
import io.wren.value.ObjModule;
import io.wren.value.ObjUpvalue;
import io.wren.value.Value;

/**
 * Represents a single running instance of a Wren program, inside a VM.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class VMContext {

	public final WrenVM vm;

	// Don't touch these.
	int index;

	int offset;
	Value condition;
	short field;
	Value receiver;
	ObjInstance instance;
	ObjClass classObj;

	// Second batch of stuff
	private CallFrame frame;
	private int stackStart;
	private int ip;
	private ObjFn fn;
	private ObjClosure closure;
	private ByteBuffer bytecode;

	/**
	 * The fiber currently being executed
	 */
	private ObjFiber fiber;

	/**
	 * The root fiber (ie, the one loaded when invoking the bytecode)
	 */
	private final ObjFiber rootFiber;

	public VMContext(WrenVM vm, ObjFiber rootFiber) {
		this.vm = vm;
		this.rootFiber = rootFiber;
		this.fiber = rootFiber;

		LOAD_FRAME();
	}

	public ObjFiber getCurrentFiber() {
		return fiber;
	}

	// TODO remove and use action methods?
	public void setCurrentFiber(ObjFiber currentFiber) {
		if (currentFiber == null) {
			throw new IllegalArgumentException("Attempted to set current fiber to null!");
		}
		STORE_FRAME();
		this.fiber = currentFiber;
		LOAD_FRAME();
	}

	private void createClass(int numFields, ObjModule module) {
		Value name = PEEK2();
		Value superclass = PEEK();

		fiber.stackTop--;

		if (!superclass.isClass()) {
			fiber.error = new Value(vm, "Class '" + name + "' cannot inherit from a non-class object.");
			return;
		}

		ObjClass superKlass = superclass.asClass();

		fiber.error = vm.validateSuperclass(name, superKlass, numFields);
		if (fiber.error != null) {
			return;
		}

		ObjClass klass = vm.newClass(superKlass, numFields, name.asString().value);
		fiber.stack[fiber.stackTop - 1] = new Value(klass);

		if (numFields == -1) {
			bindForeignClass(klass, module);
		}
	}

	private void bindForeignClass(ObjClass classObj, ObjModule module) {
		// TODO do we need to do anything?
		// See bindMethod
		ForeignClass<?> def = vm.getFFI().get(fn.module, classObj);
		classObj.foreignDef = def;
	}

	private Value bindMethod(Code instruction, int symbol, ObjModule module, ObjClass classObj, Value methodVal) {
		Method method = new Method();
		if (methodVal.isString()) { // Native method
			if (classObj.numFields == -1) {
				// This is in a foreign class - use the foreign class system
				String name = methodVal.asString().value;
				ForeignMethod fm = classObj.foreignDef.getMethod(name);

				if (fm == null) {
					return new Value(vm, "Definition for method "
							+ name + " missing in foreign class " + classObj.name + ".");
				}

				method.type = MethodType.FOREIGN;
				method.module = module;
				method.foreignMethod = fm;
			} else {
				// In a normal class, use the normal single-method system
				String name = methodVal.asString().value;
				method.type = MethodType.FOREIGN;
				method.module = module;
				method.foreignMethod = vm.getFFI().get(instruction, symbol, module, classObj, methodVal);

				if (method.foreignMethod == null) {
					return new Value(vm, "Could not find foreign method '"
							+ name + "' for class " + classObj.name
							+ " in module '" + module.name + "'.");
				}
			}
		} else {
			ObjFn methodFn = methodVal.asFnOrClosure();
			classObj.bindMethodCode(methodFn);
			method.type = MethodType.BLOCK;
			method.fn = methodVal.asObj();
		}

		if (instruction == Code.METHOD_STATIC) {
			classObj = classObj.classObj;
		}
		classObj.bindMethod(symbol, method);
		return Value.NULL;
	}

	private boolean checkArity(Value value, int numArgs) {
		ObjFn func = value.asFnOrClosure();
		if (numArgs - 1 >= func.arity) {
			return true;
		}

		fiber.error = new Value(vm, "Function expects more arguments.");
		return false;
	}

	private void callForeign(Method method, int numArgs) {
		Value inst = fiber.stack[fiber.stackTop - numArgs]; // caller
		numArgs--; // remove the caller on foreign methods.
		Value[] params = new Value[numArgs];
		for (int i = 0; i < numArgs; i++) {
			int pos = fiber.stackTop - numArgs + i;
			params[i] = fiber.stack[pos];
		}

		ForeignMethod.MethodContext ctx = new ForeignMethod.MethodContext(
				vm, method, classObj, inst, this);
		Value returned = method.foreignMethod.invokeForeign(ctx, params);

		numArgs++; // add the caller back.
		fiber.stackTop -= numArgs - 1;
		fiber.stack[fiber.stackTop - 1] = returned;
	}

	public WrenInterpretResult runStep() {
		tryRunStep();

		if (fiber == null) {
			return WrenInterpretResult.RESULT_SUCCESS;
		} else if (fiber.error != null) {
			if (!fiber.error.isString()) {
				throw new IllegalStateException("Bad error " + fiber.error);
			}
			ObjFiber initial = fiber;
			while (true) {
				ObjFiber last = fiber.caller;
				if (last == null) {
					// TODO switch to returning
					// return WrenInterpretResult.RESULT_RUNTIME_ERROR;
					System.err.println("Wren Runtime Error: " + fiber.error.toString());
					initial.dumpStack();
					throw new RuntimeException(fiber.error.toString());
				}

				if (fiber.callerIsTrying) {
					// Same as RETURN
					last.stack[last.stackTop - 1] = fiber.error;
					setCurrentFiber(last);
					break;
				} else {
					last.error = fiber.error;
					setCurrentFiber(last);
				}
			}
		}

		return null;
	}

	/**
	 * Attempt to run a single bytecode instruction.
	 *
	 * @return {@code false} if the instruction completed successfully,
	 * {@code true} if there was an error.
	 */
	private void tryRunStep() {
		Code instruction = Code.fromByte(bytecode.get(ip++));
		switch (instruction) {
			case AND:
				offset = READ_SHORT();
				condition = PEEK();

				if (condition.isFalse() || condition.isNull()) {
					ip += offset;
				} else {
					DROP();
				}
				break;
			case CALL_0:
			case CALL_1:
			case CALL_2:
			case CALL_3:
			case CALL_4:
			case CALL_5:
			case CALL_6:
			case CALL_7:
			case CALL_8:
			case CALL_9:
			case CALL_10:
			case CALL_11:
			case CALL_12:
			case CALL_13:
			case CALL_14:
			case CALL_15:
			case CALL_16:
			case SUPER_0:
			case SUPER_1:
			case SUPER_2:
			case SUPER_3:
			case SUPER_4:
			case SUPER_5:
			case SUPER_6:
			case SUPER_7:
			case SUPER_8:
			case SUPER_9:
			case SUPER_10:
			case SUPER_11:
			case SUPER_12:
			case SUPER_13:
			case SUPER_14:
			case SUPER_15:
			case SUPER_16:
				// number of arguments, including 'this'
				// so CALL_0 has this set to 1, etc
				int numArgs;
				int argsStart;

				int symbol = READ_SHORT();

				if (instruction.ordinal() < Code.SUPER_0.ordinal()) {
					numArgs = instruction.add(-Code.CALL_0.ordinal() + 1).ordinal();
					argsStart = fiber.stackTop - numArgs;

					Value self = fiber.stack[argsStart];
					classObj = vm.getInlineClass(self);
					if (classObj == null) {
						throw new IllegalStateException(String.format(
								"Object %s (class: %s) resolves to null classObj!",
								self.toString(),
								self.asObj().getClass().getName()
						));
					}
				} else {
					numArgs = instruction.add(-Code.SUPER_0.ordinal() + 1).ordinal();
					argsStart = fiber.stackTop - numArgs;

					int idx = READ_SHORT();
					classObj = fn.constants.get(idx).asClass();
				}

				if (symbol >= classObj.methods.count()
						|| classObj.methods.get(symbol).type == MethodType.NONE) {
					fiber.error = new Value(vm,
							classObj + " does not implement '" + vm.methodNames.get(symbol) + "'.");
					return;
				}

				Method method = classObj.methods.get(symbol);
				switch (method.type) {
					case BLOCK: {
						STORE_FRAME();

						fiber.appendCallFrame(method.getFn(), fiber.stackTop - numArgs);

						LOAD_FRAME();
						break;
					}
					case FOREIGN:
						callForeign(method, numArgs);
						break;
					case PRIMITIVE:
						if (method.primitiveMethod.invokePrimitive(this, fiber.stack, fiber.stackTop - numArgs, numArgs)) {
							fiber.stackTop -= (numArgs - 1);
						}
						if (fiber == null) {
							throw new IllegalStateException(
									"Current fiber cannot be null "
									+ "after method call "
									+ vm.methodNames.get(symbol));
						} else if (fiber.error != null) {
							return;
						}
						break;
					case CALL:
						if (!checkArity(fiber.stack[argsStart], numArgs)) {
							return;
						}

						STORE_FRAME();

						fiber.appendCallFrame(fiber.stack[argsStart].asObj(), fiber.stackTop - numArgs);

						LOAD_FRAME();
						break;
					case NONE:
					default:
						throw new UnreachableCodeException();
				}
				break;
			case CLASS:
				createClass(READ_BYTE(), null);
				break;
			case CLOSE_UPVALUE:
				fiber.openUpvalues.poll();
				DROP();
				break;
			case CLOSURE:
				throw new NotImplementedException();
			// break;
			case CONSTANT:
				PUSH(fn.constants.get(READ_SHORT()));
				break;
			case CONSTRUCT:
				fiber.stack[stackStart] = new Value(new ObjInstance(vm, fiber.stack[stackStart].asClass()));
				break;
			case DUP:
				Value value = PEEK();
				PUSH(value);
				break;
			case END:
				throw new IllegalStateException("END opcode should be unreachable.");
			case FALSE:
				PUSH(Value.FALSE);
				break;
			case FOREIGN_CLASS:
				// TODO set module
				createClass(-1, fn.module);
				break;
			case FOREIGN_CONSTRUCT: {
				ObjClass klass = fiber.stack[stackStart].asClass();
				ForeignClass.Constructor<?> con = klass.foreignDef.getConstructor();
				if (con == null) {
					fiber.error = new Value(vm,
							"Missing foreign constructor for " + klass.name);
					break; // Never reach this, removes warning
				}
				Obj obj = con.construct(vm, klass);
				fiber.stack[stackStart] = new Value(obj);
			}
			break;
			case IMPORT_VARIABLE: {
				Value modName = fn.constants.get(READ_SHORT());
				Value varName = fn.constants.get(READ_SHORT());

				ObjModule mod = vm.getModule(modName);
				Value val = mod.findVariable(varName.asString().value);

				PUSH(val);

				break;
			}
			case JUMP:
				offset = READ_SHORT();
				ip += offset;
				break;
			case JUMP_IF:
				offset = READ_SHORT();
				condition = POP();

				if (condition.isFalse() || condition.isNull()) {
					ip += offset;
				}
				break;
			case LOAD_FIELD:
				field = READ_BYTE();
				receiver = POP();
				instance = receiver.asInstance();
				PUSH(instance.fields[field]);
				break;
			case LOAD_FIELD_THIS:
				field = READ_BYTE();
				receiver = fiber.stack[stackStart];
				instance = receiver.asInstance();
				PUSH(instance.fields[field]);
				break;
			case LOAD_LOCAL:
				index = stackStart + READ_BYTE();
				PUSH(fiber.stack[index]);
				break;
			case LOAD_LOCAL_0:
			case LOAD_LOCAL_1:
			case LOAD_LOCAL_2:
			case LOAD_LOCAL_3:
			case LOAD_LOCAL_4:
			case LOAD_LOCAL_5:
			case LOAD_LOCAL_6:
			case LOAD_LOCAL_7:
			case LOAD_LOCAL_8:
				index = stackStart + instruction.ordinal() - Code.LOAD_LOCAL_0.ordinal();
				PUSH(fiber.stack[index]);
				break;
			case LOAD_MODULE: {
				Value val = fn.constants.get(READ_SHORT());
				ObjModule mod = vm.getOrLoadModule(val);
				if (mod == null) {
					fiber.error = new Value(vm, "Cannot load module " + val);
					return;
				}
				PUSH(new Value(mod));
				break;
			}
			case LOAD_MODULE_VAR:
				PUSH(fn.module.variables.get(READ_SHORT()));
				break;
			case LOAD_UPVALUE:
				PUSH(closure.upvalues[READ_BYTE()].value);
				break;
			case LOOP:
				offset = READ_SHORT();
				ip -= offset;
				break;
			case METHOD_INSTANCE:
			case METHOD_STATIC:
				symbol = READ_SHORT();
				classObj = PEEK().asClass();
				Value methodVal = PEEK2();
				Value val = bindMethod(instruction, symbol, fn.module, classObj, methodVal);
				if (!val.isNull()) {
					fiber.error = val;
				}
				if (fiber.error != null) {
					return;
				}
				DROP();
				DROP();
				break;
			case NULL:
				PUSH(Value.NULL);
				break;
			case OR:
				offset = READ_SHORT();
				condition = PEEK();

				if (condition.isFalse() || condition.isNull()) {
					DROP();
				} else {
					ip += offset;
				}
				break;
			case POP:
				DROP();
				break;
			case RETURN:
				Value result = POP();
				fiber.frames.pop();

				Value first = fiber.stack[stackStart];
				while (!fiber.openUpvalues.isEmpty() && fiber.openUpvalues.peek().value != first) {
					fiber.openUpvalues.poll();
				}

				if (fiber.frames.isEmpty()) {
					ObjFiber callingFiber = fiber.caller;
					fiber.caller = null;

					if (callingFiber == null) {
						fiber = null;
						return;
					}

					// TODO should this be here
					setCurrentFiber(callingFiber);

					fiber.stack[fiber.stackTop - 1] = result;
				} else {
					fiber.stack[stackStart] = result;
					fiber.stackTop = frame.stackStart + 1;
				}

				// load frame
				frame = fiber.frames.get(fiber.frames.size() - 1);
				stackStart = frame.stackStart;
				ip = frame.ip;
				fn = frame.getFn();
				closure = frame.maybeClosure();
				bytecode = fn.bytecode;
				break;
			case STORE_FIELD:
				field = READ_BYTE();
				receiver = POP();
				instance = receiver.asInstance();
				instance.fields[field] = PEEK();
				break;
			case STORE_FIELD_THIS:
				field = READ_BYTE();
				receiver = fiber.stack[stackStart];
				instance = receiver.asInstance();
				instance.fields[field] = PEEK();
				break;
			case STORE_LOCAL:
				index = stackStart + READ_BYTE();
				fiber.stack[index] = PEEK();
				break;
			case STORE_MODULE_VAR:
				fn.module.variables.set(READ_SHORT(), PEEK());
				break;
			case STORE_UPVALUE:
				ObjUpvalue[] upvalues = closure.upvalues;
				upvalues[bytecode.get(ip++)].value = fiber.stack[fiber.stackTop - 1];
				break;
			case TRUE:
				PUSH(Value.TRUE);
				break;
			default:
				break;
		}

		return;
	}

	private void LOAD_FRAME() {
		frame = fiber.frames.get(fiber.frames.size() - 1);
		stackStart = frame.stackStart;
		ip = frame.ip;
		fn = frame.getFn();
		closure = frame.maybeClosure();
		bytecode = fn.bytecode;
	}

	private void STORE_FRAME() {
		frame.ip = ip;
	}

	private void PUSH(Value value) {
		fiber.stack[fiber.stackTop++] = value;
	}

	private Value POP() {
		return fiber.stack[--fiber.stackTop];
	}

	private void DROP() {
		fiber.stackTop--;
	}

	private Value PEEK() {
		return fiber.stack[fiber.stackTop - 1];
	}

	private Value PEEK2() {
		return fiber.stack[fiber.stackTop - 2];
	}

	private short READ_BYTE() {
		short x = bytecode.get(ip++);
		if (x < 0) {
			x += 256; // make the value unsigned
		}
		return x;
	}

	private int READ_SHORT() {
		return (READ_BYTE() << 8 | READ_BYTE());
	}
}
