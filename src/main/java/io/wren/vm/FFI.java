/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wren.vm;

import io.wren.enums.Code;
import io.wren.value.*;
import java.util.*;

/**
 * Foreign Function Interface
 *
 * Provides a means for a user application to interface with a running script.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class FFI {

	private static final String METHOD_FORMAT = "%s.%s.%s";
	private static final String CLASS_FORMAT = "%s.%s";

	private final Map<String, ForeignMethod> methods;
	private final Map<String, ForeignClass<?>> classes;
	private final Deque<ModuleLoader> moduleLoaders;

	public FFI() {
		methods = new HashMap<>();
		classes = new HashMap<>();
		moduleLoaders = new ArrayDeque<>();
	}

	public void bind(String module, String klass, String name, ForeignMethod method) {
		String enc = String.format(METHOD_FORMAT, module, klass, name);
		methods.put(enc, method);
	}

	public ForeignMethod get(Code instruction, int symbol, ObjModule module, ObjClass classObj, Value methodVal) {
		String name = String.format(METHOD_FORMAT, module.name, classObj.name, methodVal.asString());
		return methods.computeIfAbsent(name, k -> {
			throw new IllegalArgumentException("Unsupported method " + k);
		});
	}

	public void bind(String module, String klass, ForeignClass<?> definition) {
		String enc = String.format(CLASS_FORMAT, module, klass);
		classes.put(enc, definition);
	}

	public ForeignClass<?> get(ObjModule module, ObjClass classObj) {
		String name = String.format(CLASS_FORMAT, module.name, classObj.name);

		ForeignClass<?> def = classes.computeIfAbsent(name, k -> {
			throw new IllegalArgumentException("Unsupported class " + k);
		});
		def.setMapping(classObj);
		return def;
	}

	/**
	 * Add a module loader, to try to load modules requested by a Wren script.
	 *
	 * @param loader The module loader to add.
	 */
	public void addModuleLoader(ModuleLoader loader) {
		addModuleLoader(loader, false);
	}

	/**
	 * Add a module loader, to try to load modules requested by a Wren script.
	 *
	 * @param loader The module loader to add.
	 */
	public void addModuleLoader(ModuleLoader loader, boolean front) {
		if (front) {
			moduleLoaders.addFirst(loader);
		} else {
			moduleLoaders.addLast(loader);
		}
	}

	/**
	 * Loads a module for the given VM.
	 *
	 * @param vm The VM to load into.
	 * @param name The name of the module to load.
	 * @return The loaded module.
	 */
	public ObjModule loadModule(WrenVM vm, String name) {
		Iterator<ModuleLoader> it = moduleLoaders.iterator();
		if (!it.hasNext()) {
			return null;
		}

		ModuleLoader last = it.next();
		while (true) {
			ModuleLoader next = it.hasNext() ? it.next() : null;

			ObjModule mod = last.load(vm, name, next);
			if (mod != null) {
				return mod;
			}

			last = next;

			if (last == null) {
				break;
			}
		}

		return null;
	}

	public interface ModuleLoader {

		/**
		 * Load a given module by name.
		 *
		 * @param name The name of the module to load.
		 * @param next The next ModuleLoader to try, or {@code null} if this is
		 * the last module loader available. This can be used to modify module
		 * names, however if you can't load a module you should return
		 * {@code null} instead.
		 * @return The module requested, or {@code null} if this loader cannot
		 * find the specified module.
		 */
		ObjModule load(WrenVM vm, String name, ModuleLoader next);
	}

}
