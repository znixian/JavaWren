package io.wren.vm;

import io.wren.value.ObjClass;
import io.wren.value.ObjString;

import java.util.HashMap;
import java.util.Map;

public class StringPool {
	private final Map<String, ObjString> strings;
	private final WrenVM vm;

	public StringPool(WrenVM vm) {
		this.vm = vm;
		strings = new HashMap<>();
	}

	public void updateClasses(ObjClass klass) {
		for (ObjString s : strings.values()) {
			s.classObj = klass;
		}
	}

	public ObjString get(String from) {
		return strings.computeIfAbsent(from, this::createString);
	}

	private ObjString createString(String s) {
		return new ObjString(vm, s);
	}
}
