/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wren.vm;

import io.wren.value.ForeignMethod;
import io.wren.value.Obj;
import io.wren.value.ObjClass;
import io.wren.value.Value;
import java.util.*;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ForeignClass<T extends Obj> {
	// TODO: throw exception if method is added and not used

	private final Map<String, ForeignMethod> methods;
	private final Constructor<T> constructor;

	private ObjClass mapping;

	public ForeignClass() {
		this(null);
	}

	public ForeignClass(Constructor<T> constructor) {
		methods = new HashMap<>();
		this.constructor = constructor;
	}

	/**
	 * Bind to a foreign method on this class.
	 *
	 * @param name The name of the method to bind.
	 * @param value The method to bind.
	 */
	public void addMethod(String name, ForeignMethod value) {
		methods.put(name, value);
	}

	/**
	 * Binds a method call to a given {@link Mapping}, where the mapping accepts
	 * the java backing type for the method.
	 *
	 * @param name The name of the method to map.
	 * @param mapping The mapping to map to.
	 */
	public void bind(String name, Mapping<T> mapping) {
		addMethod(name, (ctx, params) -> {
			@SuppressWarnings("unchecked")
			T instance = (T) ctx.getInstance().asObj();
			return mapping.call(instance, ctx, params);
		});
	}

	/**
	 * Bind a method for a custom type to a method that can return the foreign
	 * method for a given instance. This is designed to be used for Lambdas:
	 *
	 * <pre> {@code
	 * def.bind("greeting", hi -> hi::greeting);
	 * }</pre>
	 *
	 * @param name The name of the method to bind to.
	 * @param mapping The function that can return a foreign method for a given
	 * instance.
	 */
	public void bind(String name, LambdaMapping<T> mapping) {
		addMethod(name, (ctx, params) -> {
			@SuppressWarnings("unchecked")
			T instance = (T) ctx.getInstance().asObj();
			return mapping.get(instance).invokeForeign(ctx, params);
		});
	}

	public ForeignMethod getMethod(String name) {
		return methods.get(name);
	}

	public Constructor<T> getConstructor() {
		return constructor;
	}

	public ObjClass getMapping() {
		return mapping;
	}

	public void setMapping(ObjClass mapping) {
		this.mapping = mapping;
	}

	public interface Mapping<T extends Obj> {

		Value call(T instance, ForeignMethod.MethodContext ctx, Value[] params);
	}

	public interface LambdaMapping<T extends Obj> {

		ForeignMethod get(T instance);
	}

	public interface Constructor<T extends Obj> {

		T construct(WrenVM vm, ObjClass type);
	}
}
