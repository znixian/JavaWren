
import io.wren.debug.Disassembler;
import io.wren.enums.WrenInterpretResult;
import io.wren.utils.Wren;
import io.wren.value.*;
import io.wren.vm.WrenVM;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.junit.Assert.*;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestCustomClasses {

	@Test
	public void testHackyInjectStaticMethod() {
		// Note for anyone wanting to use this themselves:
		// Please, use the Foreign Function Interface (FFI)!
		// See testForeignMethod for an example of this

		final String PROGRAM = "Testing.mark()\n";
		WrenVM vm = new WrenVM();

		AtomicBoolean bool = new AtomicBoolean(false);

		// Hacky way to get it into the currently running module
		ObjClass testing = vm.apiDefineClass(vm.getCoreModule(), "Testing");
		testing.classObj.bindStaticMethod(vm.methodNames.ensure("mark()"), (ctx, stack, stackStart, numArgs) -> {
			bool.set(!bool.get());
			return Wren.RETURN(stack, stackStart);
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
//		Disassembler dis = new Disassembler(fi);

		assertEquals(bool.get(), false);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(bool.get(), true);
	}

	@Test
	public void testForeignMethod() {
		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static hello()\n"
				+ "}\n"
				+ "Testing.hello()\n";
		WrenVM vm = new WrenVM();

		AtomicBoolean bool = new AtomicBoolean(false);

		// Inject into module "mod", class "Testing" and method "hello()"
		// Make sure you run this BEFORE you load the module, as the
		// definition is loaded when the bytecode is parsed.
		vm.getFFI().bind("mod", "Testing", "hello()", (ctx, params) -> {
			bool.set(!bool.get());
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
//		Disassembler dis = new Disassembler(fi);

		assertEquals(false, bool.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(true, bool.get());
	}

	@Test
	public void testConstructObject() {
		final String PROGRAM
				= "class Testing {\n"
				+ "\tconstruct new(a, b) {\n"
				+ "\t\t_a = a\n"
				+ "\t\t_b = b\n"
				+ "\t}\n"
				+ "\thello() {\n"
				+ "\t\tSystem.print(_a + _b)\n"
				+ "\t}\n"
				+ "\tforeign static hello()\n"
				+ "}\n"
				+ "Testing.new(1,2).hello()\n"
				+ "Testing.hello().hello()\n"
				+ "";
		WrenVM vm = new WrenVM();

		System.out.println("program: " + PROGRAM);

		// Inject into module "mod", class "Testing" and method "hello()"
		// Make sure you run this BEFORE you load the module, as the
		// definition is loaded when the bytecode is parsed.
		vm.getFFI().bind("mod", "Testing", "hello()", (ctx, params) -> {
			ObjModule module = ctx.getMethod().module;
			ObjClass c = module.findVariable("Testing").asClass();

			System.out.println("klass: " + c + " vs " + ctx.getKlass());
			ObjInstance inst = new ObjInstance(ctx.getVm(), c);
			inst.fields[0] = new Value(10);
			inst.fields[1] = new Value(20);
			return new Value(inst);
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
		Disassembler dis = new Disassembler(fi);

		assertNotNull("Program must compile", fi);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
	}

	@Test
	public void testForeignBacking() {
		// Note: When using a foreign backing, you should be using a foreign
		// class. Haven't written tests for those yet TODO.

		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static inst()\n"
				//				+ "foreign static print(val)\n"
				+ "foreign hello()\n"
				+ "}\n"
				+ "Testing.inst().hello()\n";
//				+ "Testing.print(Testing.inst())//.hello()\n";
		WrenVM vm = new WrenVM();

		class ObjTest extends Obj {

			public boolean hit;

			public ObjTest(WrenVM vm) {
				super(vm);
			}

			public Value update(ForeignMethod.MethodContext ctx, Value[] params) {
				classObj = ctx.getInstance().asClass();
				assertEquals(
						ctx.getMethod().module.findVariable("Testing").asClass(),
						classObj
				);
				return new Value(this);
			}
		}

		ObjTest test = new ObjTest(vm);
		vm.getFFI().bind("mod", "Testing", "inst()", test::update);

		vm.getFFI().bind("mod", "Testing", "hello()", (ctx, params) -> {
			ObjTest obj = (ObjTest) ctx.getInstance().asObj();
			assertEquals(test, obj);
			obj.hit = true;

			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertFalse(test.hit);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(test.hit);
	}
}
