/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.value.*;
import io.wren.vm.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestExtendForeignClass {

	private boolean hit;

	@Test
	public void testForeignExtendForeign() {
		// TODO update to be inline with Wren's foreign class rules

		final String PROGRAM
				= "foreign class Base {\n"
				+ "foreign hello()\n"
				+ "}\n"
				+ "foreign class Testing is Base {\n"
				+ "foreign static inst\n"
				+ "}\n"
				+ "Testing.inst.hello()\n";
		WrenVM vm = new WrenVM();

		ObjTesting inst = new ObjTesting(vm);

		ForeignClass<ObjBase> base = new ForeignClass<>();
		base.bind("hello()", i -> i::hello);
		vm.getFFI().bind("mod", "Base", base);

		ForeignClass<ObjTesting> testing = new ForeignClass<>();
		testing.addMethod("inst", inst::update);
		vm.getFFI().bind("mod", "Testing", testing);

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		try {
			vm.runInterpreter(fi);
			fail("Foreign class was able to extend another foreign class!");
		} catch (RuntimeException ex) {
			assertEquals(RuntimeException.class, ex.getClass());
			assertEquals("Class 'Testing' cannot inherit from foreign class 'Base'.", ex.getMessage());
		}
	}

	private class ObjBase extends Obj {

		public ObjBase(WrenVM vm) {
			super(vm);
		}

		public Value hello(ForeignMethod.MethodContext ctx, Value[] params) {
			hit = true;
			return Value.NULL;
		}
	}

	private class ObjTesting extends ObjBase {

		public ObjTesting(WrenVM vm) {
			super(vm);
		}

		public Value update(ForeignMethod.MethodContext ctx, Value[] params) {
			classObj = ctx.getInstance().asClass();
			return new Value(this);
		}
	}
}
