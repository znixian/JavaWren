/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.ForeignClass;
import io.wren.vm.WrenVM;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestForeignClass {

	@Test
	public void testForeignBacking() {
		final String PROGRAM
				= "foreign class Testing {\n"
				+ "foreign static inst()\n"
				+ "foreign hello()\n"
				+ "}\n"
				+ "Testing.inst().hello()\n";
		WrenVM vm = new WrenVM();

		ObjTest test = new ObjTest(vm);

		ForeignClass<ObjTest> klass = new ForeignClass<>();
		klass.addMethod("inst()", test::update);
		klass.bind("hello()", i -> i::hello);
		vm.getFFI().bind("mod", "Testing", klass);

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertFalse(test.hit);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(test.hit);
	}

	@Test
	public void testForeignConstructor() {
		final String PROGRAM
				= "foreign class Testing {\n"
				+ "construct new() {\n"
				+ "hello()\n"
				+ "}\n"
				+ "foreign hello()\n"
				+ "}\n"
				+ "Testing.new()\n";
		WrenVM vm = new WrenVM();

		ObjTest test = new ObjTest(vm);

		ForeignClass<ObjTest> klass = new ForeignClass<>(ObjTest::new);
		klass.bind("hello()", i -> test::hello);
		vm.getFFI().bind("mod", "Testing", klass);

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertFalse(test.hit);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(test.hit);
	}

	@Test
	public void testPureForeignConstructor() {
		final String PROGRAM
				= "foreign class Testing {\n"
				+ "foreign construct new()\n"
				+ "foreign hello()\n"
				+ "}\n"
				+ "Testing.new().hello()\n";
		WrenVM vm = new WrenVM();

		ObjTest test = new ObjTest(vm);

		ForeignClass<ObjTest> klass = new ForeignClass<>(ObjTest::new);
		klass.bind("init new()", i -> i::construct);
		klass.bind("hello()", i -> test::hello);
		vm.getFFI().bind("mod", "Testing", klass);

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertFalse(test.hit);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(test.hit);
	}

	class ObjTest extends Obj {

		public boolean hit;

		public ObjTest(WrenVM vm) {
			super(vm);
		}

		public ObjTest(WrenVM vm, ObjClass type) {
			super(vm);
			classObj = type;
		}

		public Value update(ForeignMethod.MethodContext ctx, Value[] params) {
			classObj = ctx.getInstance().asClass();
			assertEquals(
					ctx.getMethod().module.findVariable("Testing").asClass(),
					classObj
			);
			return new Value(this);
		}

		public Value construct(ForeignMethod.MethodContext ctx, Value[] params) {
			// TODO custom init logic

			return new Value(this);
		}

		public Value hello(ForeignMethod.MethodContext ctx, Value[] params) {
			hit = true;

			return Value.NULL;
		}
	}
}
