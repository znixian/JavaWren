/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.debug.Disassembler;
import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.WrenVM;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestPrimitives {

	@Test
	public void testNumberEquals() {
		test(true, "Testing.set = 1 == 1\n");
		test(false, "Testing.set = 1 == 2\n");
	}

	@Test
	public void testNumberNotEquals() {
		test(true, "Testing.set = 1 != 2\n");
		test(false, "Testing.set = 1 != 1\n");
	}

	public void test(boolean target, String PROGRAM) {
		PROGRAM = ""
				+ "class Testing {\n"
				+ "foreign static set=(val)\n"
				+ "}\n"
				+ PROGRAM;

		WrenVM vm = new WrenVM();

		AtomicBoolean val = new AtomicBoolean(!target);

		vm.getFFI().bind("mod", "Testing", "set=(_)", (ctx, params) -> {
			val.set(params[0].isTrue());
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertEquals(!target, val.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(target, val.get());
	}
}
