/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.debug.Disassembler;
import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.WrenVM;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestLoadModule {

	@Test
	public void testPreloadedImport() {
		final String PRE
				= "class Testing {\n"
				+ "foreign static mark()\n"
				+ "}\n";

		final String PROGRAM
				= "import \"Other\" for Testing\n"
				+ "Testing.mark()\n";
		WrenVM vm = new WrenVM();

		AtomicBoolean val = new AtomicBoolean();

		vm.getFFI().bind("Other", "Testing", "mark()", (ctx, params) -> {
			val.set(true);
			return Value.NULL;
		});

		WrenInterpretResult res = vm.interpret("Other", PRE);
		assertEquals(res, WrenInterpretResult.RESULT_SUCCESS);

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
		new Disassembler(fi);

		assertFalse(val.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(val.get());
	}

	@Test
	public void testModules() {
		final String PRE
				= "class Testing {\n"
				+ "foreign static mark()\n"
				+ "}\n";

		final String PROGRAM
				= "import \"Other\" for Testing\n"
				+ "Testing.mark()\n";
		WrenVM vm = new WrenVM();

		AtomicBoolean val = new AtomicBoolean();

		vm.getFFI().bind("Other", "Testing", "mark()", (ctx, params) -> {
			val.set(true);
			return Value.NULL;
		});

		vm.getFFI().addModuleLoader((tvm, name, next) -> {
			if (!"Other".equals(name)) {
				return null;
			}

			return tvm.interpretModule(name, PRE);
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertFalse(val.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(val.get());
	}
}
