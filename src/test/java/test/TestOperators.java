package test;

import io.wren.enums.WrenInterpretResult;
import io.wren.value.ForeignMethod.MethodContext;
import io.wren.value.*;
import io.wren.vm.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestOperators {

	@Test
	public void testBiInfix() {
		runTest(4.95, "Testing.inst.count = 5 - 0.05");
	}

	@Test
	public void testBiPrefix() {
		runTest(-0.5, "Testing.inst.count = - 0.5");
	}

	@Test
	public void testPrefix() {
		runTest(1234, "Testing.inst.count = ~Testing.inst");

		// Also test with a infix, to check precedence
		runTest(1, "Testing.inst.count = ~Testing.inst - 1233");
	}

	@Test
	public void testInfix() {
		runTest(777, "Testing.inst.count = 123 + 654");
	}

	private void runTest(double result, String PROGRAM) {
		final String BASE = ""
				+ "foreign class Testing {\n"
				+ "foreign static inst\n"
				+ "foreign count\n"
				+ "foreign count=(arg)\n"
				+ "foreign ~\n"
				+ "}\n";

		WrenVM vm = new WrenVM();

		TestValue inst = new TestValue(vm);

		ForeignClass<TestValue> def = new ForeignClass<>();
		def.bind("count", i -> i::getCount);
		def.bind("count=(_)", i -> i::setCount);
		def.addMethod("inst", inst::bind);
		def.addMethod("~", inst::tilde);
		vm.getFFI().bind("mod", "Testing", def);

		ObjFiber fi = vm.loadModule("mod", BASE + PROGRAM + "\n");
		// new Disassembler(fi);

		assertEquals(0, inst.value, 0);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(result, inst.value, 0.0001);
	}

	private class TestValue extends Obj {
		public double value;

		public TestValue(WrenVM vm) {
			super(vm);
		}

		public Value bind(MethodContext ctx, Value[] args) {
			classObj = ctx.getInstance().asClass();
			return new Value(this);
		}

		// fun stuff

		public Value getCount(MethodContext ctx, Value[] args) {
			return new Value(value);
		}

		public Value setCount(MethodContext ctx, Value[] args) {
			value = args[0].asDouble();
			return Value.NULL;
		}

		public Value tilde(MethodContext ctx, Value[] args) {
			return new Value(1234);
		}
	}
}
