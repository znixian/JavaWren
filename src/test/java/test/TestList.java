/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.debug.Disassembler;
import io.wren.enums.WrenInterpretResult;
import io.wren.value.ObjFiber;
import io.wren.value.Value;
import io.wren.vm.WrenVM;
import java.util.concurrent.atomic.AtomicInteger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestList {

	@Test
	public void testListAccess() {
		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static set=(val)\n"
				+ "}\n"
				+ "var l = [0,1,2,3]\n"
				+ "Testing.set = l[2]\n";
		WrenVM vm = new WrenVM();

		AtomicInteger num = new AtomicInteger();

		vm.getFFI().bind("mod", "Testing", "set=(_)", (ctx, params) -> {
			num.set(params[0].asInt());
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
		new Disassembler(fi);

		assertEquals(0, num.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(2, num.get());
	}
}
