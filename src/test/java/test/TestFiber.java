/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.*;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestFiber {

	@Test
	public void testYield() {
		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static append=(val)\n"
				+ "}\n"
				+ "var fiber = Fiber.new {\n"
				+ "for(i in 1..3) {\n"
				+ "Testing.append = i.toString\n"
				+ "Fiber.yield(i)\n"
				+ "}\n"
				+ "}\n"
				+ "while(!fiber.isDone) {\n"
				+ "var n = fiber.call()\n"
				+ "if(n == null) {\n"
				+ "break\n"
				+ "}\n"
				+ "System.print(n)\n"
				+ "Testing.append = String.fromCodePoint(n + 96)\n"
				+ "}\n";
		WrenVM vm = new WrenVM();

		AtomicReference<String> str = new AtomicReference<>("");

		vm.getFFI().bind("mod", "Testing", "append=(_)", (ctx, params) -> {
			str.set(str.get() + "," + params[0].asString().value);
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertEquals("", str.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(",1.0,a,2.0,b,3.0,c", str.get());
	}

	@Test
	public void testTry() {
		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static res=(val)\n"
				+ "}\n"
				+ "var fiber = Fiber.new {\n"
				+ "null.hello()\n"
				+ "}\n"
				+ "Testing.res = fiber.try()\n";
		WrenVM vm = new WrenVM();

		AtomicReference<String> str = new AtomicReference<>("");

		vm.getFFI().bind("mod", "Testing", "res=(_)", (ctx, params) -> {
			str.set(params[0].asString().value);
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertEquals("", str.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals("instance of Null metaclass does not implement 'hello()'.", str.get());
	}
}
