/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.debug.Disassembler;
import io.wren.enums.WrenInterpretResult;
import io.wren.value.ObjFiber;
import io.wren.value.Value;
import io.wren.vm.WrenVM;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestStrings {

	@Test
	public void testSubscript() {
		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static set=(val)\n"
				+ "}\n"
				+ "var s = \"abcdef\"\n"
				+ "Testing.set = s[2]\n";
		WrenVM vm = new WrenVM();

		AtomicReference<String> str = new AtomicReference<>("");

		vm.getFFI().bind("mod", "Testing", "set=(_)", (ctx, params) -> {
			str.set(params[0].asString().value);
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
		new Disassembler(fi);

		assertEquals("", str.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals("c", str.get());
	}
}
