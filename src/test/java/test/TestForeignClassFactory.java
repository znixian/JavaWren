/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestForeignClassFactory {

	private ForeignClass<ObjTestingFactory> factory;
	private ForeignClass<ObjTesting> testing;
	private boolean hit;

	@Test
	public void testForeignInitialization() {
		final String PROGRAM
				= "foreign class Testing {\n"
				+ "foreign hello()\n"
				+ "}\n"
				+ "foreign class TestingFactory {\n"
				+ "foreign construct new()\n"
				+ "foreign build\n"
				+ "}\n"
				+ "TestingFactory.new().build.hello()\n";
		WrenVM vm = new WrenVM();

		factory = new ForeignClass<>(ObjTestingFactory::new);
		factory.bind("init new()", (instance, ctx, params) -> ctx.getInstance());
		factory.bind("build", i -> i::build);
		vm.getFFI().bind("mod", "TestingFactory", factory);

		testing = new ForeignClass<>();
		testing.bind("hello()", i -> i::hello);
		vm.getFFI().bind("mod", "Testing", testing);

		ObjFiber fi = vm.loadModule("mod", PROGRAM);

		assertFalse(hit);
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertTrue(hit);
	}

	private class ObjTesting extends Obj {

		public ObjTesting(WrenVM vm) {
			super(vm);
			classObj = testing.getMapping();
		}

		public Value hello(ForeignMethod.MethodContext ctx, Value[] params) {
			hit = true;
			return Value.NULL;
		}
	}

	private class ObjTestingFactory extends Obj {

		public ObjTestingFactory(WrenVM vm, ObjClass def) {
			super(vm);
			classObj = def;
		}

		public Value build(ForeignMethod.MethodContext ctx, Value[] params) {
			return new Value(new ObjTesting(vm));
		}
	}
}
