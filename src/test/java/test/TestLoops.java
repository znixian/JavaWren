/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import io.wren.debug.Disassembler;
import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.*;

import java.util.concurrent.atomic.AtomicInteger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class TestLoops {

	@Test
	public void testNestedLoops() {
		final String PROGRAM
				= "class Testing {\n"
				+ "foreign static count()\n"
				+ "}\n"
				+ "for (i in 1..10) {\n"
				+ "for (n in 1..10) {\n"
				+ "Testing.count()\n"
				+ "}\n"
				+ "}\n";
		WrenVM vm = new WrenVM();

		AtomicInteger count = new AtomicInteger();

		vm.getFFI().bind("mod", "Testing", "count()", (ctx, params) -> {
			count.addAndGet(1);
			return Value.NULL;
		});

		ObjFiber fi = vm.loadModule("mod", PROGRAM);
		new Disassembler(fi);

		assertEquals(0, count.get());
		assertEquals(vm.runInterpreter(fi), WrenInterpretResult.RESULT_SUCCESS);
		assertEquals(100, count.get());
	}
}
