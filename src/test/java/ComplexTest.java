
import io.wren.debug.Disassembler;
import io.wren.value.ObjFiber;
import io.wren.vm.WrenVM;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ComplexTest {

	public static final String PROGRAM
		= "var counter = 0\n"
		+ "for(i in 1..10)"
		+ "counter = counter + i\n"
		+ "System.print(counter)";

	@Test
	public void testComplexAOT() {
		WrenVM vm = new WrenVM();
		ObjFiber fiber = vm.loadModule("hi!", PROGRAM);
		Disassembler dis = new Disassembler(fiber);
		vm.runInterpreter(fiber);
	}
}
